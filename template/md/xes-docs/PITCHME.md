---
@title[What is a XES File?]

##### What is a XES File?

Main source: [XES Standard](http://xes-standard.org/)

The XES standard defines a grammar for a tag-based language whose aim is to provide designers of information systems with a unified and extensible methodology for capturing systems behaviors by means of event logs and event streams is defined in the XES standard.

---
@title[Anatomy of a XES File]

@fa[arrow-down text-black]

@snap[north text-10]
Anatomy of a XES File
@snapend

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<log xes.version="1849.2016" xes.features="">
	<extension name="Lifecycle" prefix="lifecycle" uri="http://www.xes-standard.org/lifecycle.xesext"/>
	<extension name="Organizational" prefix="org" uri="http://www.xes-standard.org/org.xesext"/>
	...
	<global scope="trace">
		<string key="concept:name" value="__INVALID__"/>
	</global>
	<global scope="event">
		<string key="concept:name" value="__INVALID__"/>
		<string key="concept:instance" value="__INVALID__"/>
		...
	</global>
	<string key="concept:name" value="Filtered D1 log"/>
	<trace>
		<string key="concept:name" value="1"/>
		<event>
			<string key="org:group" value="Group -"/>
			<date key="time:timestamp" value="1970-01-02T12:24:45.453+01:00"/>
			...
		</event>
    </trace> 
</log>
```

@[1](XML Definition)
@[2,23](XES Log Structure)
@[3,5-13](XES Attributes)
@[15-22](XES Trace Structure)
@[17-21](XES Event Structure (**Typed** Elements))