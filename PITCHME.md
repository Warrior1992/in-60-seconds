---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Introduction]

@snap[north text-20]

## XeScala

@snapend

@snap[midpoint text-15]

#### A Scala Plugin for<br> XES Files

@snapend

@snap[south text-8]
###### Eugenio **Liso** --- Alessandro **Marino**
@snapend

---?include=template/md/xes-docs/PITCHME.md

---?include=template/md/list-content/PITCHME.md

---?include=template/md/image/PITCHME.md

---?include=template/md/code-presenting/PITCHME.md